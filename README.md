# 2020-2021 DAM-C

Repositori de programació (M03) del grup 1rDAM-C  del curs 2020/21.
Aquí trobareu els diferents projectes que anirem treballant al llarg del curs. Estaran organitzats per carpetes; una per cada UF.
Podeu sincronitzar el repositori amb el vostre .git local. D'aquesta manera, sempre tindreu l'última versió actualitzada.
Per més informació sobre git podeu consultar el següent [enllaç](https://docs.google.com/document/d/1JthynfEQFJL3rBVygwb60CFIOK7CuIXABoGliIhN534/edit#)  