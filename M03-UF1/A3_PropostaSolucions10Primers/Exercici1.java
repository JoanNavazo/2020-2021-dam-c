package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 1
Descripcio: Algorisme que llegeix un n�mero per teclat, el multiplica per 3 i mostra el resultat.
Autor: David L�pez 
 */
import java.util.Scanner;

public class Exercici1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		int num = 0;
		
		System.out.print("Introdueix un numero: ");
		num = reader.nextInt();
		
		reader.close();
		
		System.out.print("Resultat multiplicat per 3: "+(num*3));
	}

}
